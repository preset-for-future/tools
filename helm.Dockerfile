#FROM registry.suse.com/bci/golang:1.17 AS helm
#ENV helm_version=v3.8.0
#RUN zypper -n install git
#RUN git -C / clone --branch release-$helm_version --depth=1 https://github.com/rancher/helm
#RUN make -C /helm

FROM registry.access.redhat.com/ubi8/ubi

RUN yum install --setopt=tsflags=nodocs --nogpgcheck -y wget ca-certificates gettext git jq && \
    yum clean all && \
    rm -rf /var/cache

ENV kubectl_version=v1.21.14
RUN curl -sSL https://storage.googleapis.com/kubernetes-release/release/$kubectl_version/bin/linux/amd64/kubectl \
    -o /usr/local/bin/kubectl && \
    chmod a+x /usr/local/bin/kubectl

ENV kapp_version=v0.54.1
RUN curl -sSL https://github.com/vmware-tanzu/carvel-kapp/releases/download/$kapp_version/kapp-linux-amd64 \
    -o /usr/local/bin/kapp && \
    chmod a+x /usr/local/bin/kapp

ENV yq_version=v4.30.6
RUN curl -sSL https://github.com/mikefarah/yq/releases/download/$yq_version/yq_linux_amd64 \
    -o /usr/local/bin/yq && chmod a+x /usr/local/bin/yq

ENV werf_version=1.2.277
RUN curl -sSL https://tuf.werf.io/targets/releases/${werf_version}/linux-amd64/bin/werf \
    -o /usr/local/bin/werf && \
    chmod a+x /usr/local/bin/werf

ENV helm_version=v3.10.3
RUN curl -sSL https://get.helm.sh/helm-${helm_version}-linux-amd64.tar.gz \
    -o helm-linux-amd64.tar.gz && \
    mkdir -p "/tmp/helm" && \
    tar xf "helm-linux-amd64.tar.gz" -C "/tmp/helm" && \
    cp "/tmp/helm/linux-amd64/helm" "/usr/local/bin/helm" && \
    chmod a+x "/usr/local/bin/helm" && \
    rm -rf /tmp/helm

#COPY --from=helm ./helm/bin/helm /usr/local/bin/

RUN helm plugin install https://github.com/chartmuseum/helm-push || true
