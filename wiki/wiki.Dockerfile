FROM python:3.9.5-buster
COPY requirements.txt .
RUN pip install -r requirements.txt && \
    apt-get update && \
    apt-get install -y \
        gettext-base && \
    rm -rf /var/lib/apt/lists/*
