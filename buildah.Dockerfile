FROM quay.io/buildah/stable:v1.33.2
RUN sed -i 's/short-name-mode="enforcing"/short-name-mode="permissive"/g' /etc/containers/registries.conf
