#FROM registry.suse.com/bci/golang:1.17 AS helm
#ENV helm_version=v3.8.0
#RUN zypper -n install git
#RUN git -C / clone --branch release-$helm_version --depth=1 https://github.com/rancher/helm
#RUN make -C /helm

FROM registry.access.redhat.com/ubi8/ubi

RUN yum install --setopt=tsflags=nodocs --nogpgcheck -y wget ca-certificates gettext git && \
    yum clean all && \
    rm -rf /var/cache/yum

ENV kubectl_version=v1.21.0
RUN curl -sSL https://storage.googleapis.com/kubernetes-release/release/$kubectl_version/bin/linux/amd64/kubectl \
    -o /usr/local/bin/kubectl && \
    chmod a+x /usr/local/bin/kubectl

ENV kapp_version=v0.49.0
RUN curl -sSL https://github.com/vmware-tanzu/carvel-kapp/releases/download/$kapp_version/kapp-linux-amd64 \
    -o /usr/local/bin/kapp && \
    chmod a+x /usr/local/bin/kapp

ENV yq_version=v4.28.1
RUN curl -sSL https://github.com/mikefarah/yq/releases/download/$yq_version/yq_linux_amd64 \
    -o /usr/local/bin/yq && chmod a+x /usr/local/bin/yq

ENV werf_version=1.2.180
RUN curl -sSL https://tuf.werf.io/targets/releases/${werf_version}/linux-amd64/bin/werf \
    -o /usr/local/bin/werf && \
    chmod a+x /usr/local/bin/werf

ENV helm_version=v3.10.1
RUN curl -sSL https://get.helm.sh/helm-${helm_version}-linux-amd64.tar.gz \
    -o helm-linux-amd64.tar.gz && \
    mkdir -p "/tmp/helm" && \
    tar xf "helm-linux-amd64.tar.gz" -C "/tmp/helm" && \
    cp "/tmp/helm/linux-amd64/helm" "/usr/local/bin/helm" && \
    chmod a+x "/usr/local/bin/helm" && \
    rm -rf /tmp/helm

ENV helmfile_version=0.149.0
RUN curl -sSL https://github.com/helmfile/helmfile/releases/download/v${helmfile_version}/helmfile_${helmfile_version}_linux_amd64.tar.gz \
    -o helmfile.tar.gz && \
    mkdir -p /tmp/helmfile && \
    tar xf "helmfile.tar.gz" -C "/tmp/helmfile" && \
    cp "/tmp/helmfile/helmfile" "/usr/local/bin/helmfile" && \
    chmod a+x "/usr/local/bin/helmfile" && \
    rm -rf /tmp/helmfile

RUN echo "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSURhekNDQWxPZ0F3SUJBZ0lRUzhkZThJVXUrYURtNFVNQStVM2xuVEFOQmdrcWhraUc5dzBCQVFzRkFEQkEKTVJVd0V3WURWUVFLRXd4Q1lXNTZZV2tnUTJ4dmRXUXhKekFsQmdOVkJBTVRIa0poYm5waGFTQkRiRzkxWkNCSApaVzVsY21GMFpXUWdVbTl2ZENCRFFUQWVGdzB5TXpBME1qVXhOelU0TlRCYUZ3MHlOREEwTWpReE56VTROVEJhCk1FQXhGVEFUQmdOVkJBb1RERUpoYm5waGFTQkRiRzkxWkRFbk1DVUdBMVVFQXhNZVFtRnVlbUZwSUVOc2IzVmsKSUVkbGJtVnlZWFJsWkNCU2IyOTBJRU5CTUlJQklqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FROEFNSUlCQ2dLQwpBUUVBekVtdHpaZFVXTFRhR0VFWWp2aVlPUk1Ldks3Y0pGMzRCejNKQlFGQnh0cWNUOTJERTNSQlNJMnFScHk0CjdkVzBETXA3QnBMSFlDMTFFeVBVZXFwT2I2bW9pdU5EY2RLeVRCY2dQUHFCd3UvWkhGR1U1THBaMVdSay9VRW0KRUdsc09OWnNVQWpkTnRlNVZ3MDZONWhtQ2o4UXBHRktGekxsNUZVUXliTUdkMjJGTWZ5T3ZlU2tQRmxzS0EwMwpzUjA0a2MxZDNBMlVvS1RlNWZJZmZZSjBIaVZFZ3Qrb1pSN3IxMDh3bW9QaVpmZkFNMy82M2svbHVvVTRpVDVhCmhYUWNXUVpjbnBoT0NibDJqVG1JM010blRzdUU3VzNBRVN4a2IrRHVnN0NmYllJZUh2MjZjZG5nbTlGcmZBRUcKcno2cWQ1K3pCNkcwVGVVMWszdGdQbmpmWXdJREFRQUJvMkV3WHpBT0JnTlZIUThCQWY4RUJBTUNBZ1F3SFFZRApWUjBsQkJZd0ZBWUlLd1lCQlFVSEF3RUdDQ3NHQVFVRkJ3TUNNQThHQTFVZEV3RUIvd1FGTUFNQkFmOHdIUVlEClZSME9CQllFRkdyZHZiWE1MYkJkS3hkeHpCMUNhVXRveDRXRk1BMEdDU3FHU0liM0RRRUJDd1VBQTRJQkFRQXIKejRPVlZlbFo0cWxabm9lRWd1TzVxSlVSb0pONmM4aFFtY1djRHdValpJRE4xRzU0cVdqelRuOFFsV09TTVRDZQpDek53Ry9YWUR1RythbkpneDArM0xkTXphV2hUdy9RWDNxY0JQZ2VhcU5LS0VNSnNiM3BDbGUrellsVGpwUnFJCnk0b3pZa0NzVndSY2lNeXlwazBlbzBSRlpGL3V6bjQ1VkVYV0psWU5XZlkzVWpkOWFGYngzSWc1dmdLbzd2L2cKTXZVTEN2Q2xST2E0ejhsN2NjMklUUFVHYXlDNTlhbCs3d2VINjZFdDFFVVpmMU1wOVdURXhKL0U1UmNkenk3ZQpldjA2WHdreGFoZXFoRDZ5Ly9wT2l6K2ZFRU8ranJUSUtTM3BESUMwQ2RGSWErS3BmNVFOVHg4bllUenduTlF1Cm1BUTdkRkptM3ZKNFhQSmI5NGlKCi0tLS0tRU5EIENFUlRJRklDQVRFLS0tLS0K" | base64 -d > /etc/pki/ca-trust/source/anchors/vault.crt && update-ca-trust

RUN echo "-----BEGIN CERTIFICATE-----MIIEiDCCAvCgAwIBAgIBATANBgkqhkiG9w0BAQsFADA1MRMwEQYDVQQKDApQQ0JMLkxPQ0FMMR4wHAYDVQQDDBVDZXJ0aWZpY2F0ZSBBdXRob3JpdHkwHhcNMjAwNzIwMTYzNjU0WhcNNDAwNzIwMTYzNjU0WjA1MRMwEQYDVQQKDApQQ0JMLkxPQ0FMMR4wHAYDVQQDDBVDZXJ0aWZpY2F0ZSBBdXRob3JpdHkwggGiMA0GCSqGSIb3DQEBAQUAA4IBjwAwggGKAoIBgQC1r7FMCPbr0sn4LM+EtzfP3/ifjMkOiNJ3NOflm52fOSCPccZVfAcPbfsM+FYer1WNQTmNrCYsGH2YOH87Folqb6wqAiY3geaTbn+acgonJFg1nA1Uv3tyOZBifwFdPh8rvozFpvTK1WfiXYj4wQQzKTAdYcZhcnmP9ty6l+b+oCcOhsAqZWGSu6KSz64R9JGyIDFuj97aOsUC7JAVkeed3+mbXG83k1pxWGRatezBwhKJdoHLItlN9Clkz2g/dJo9ec/kGaayYQg4WGi3UXbMUwn73afz5+9pchr0UZb1qg6LfzKHTnVJrn+w3slgWycfAm7tLCSMvOauqDWzls4s8DfXLI4UagxYtn6heRRhtdhNYjnxpJdOPK2uFF8rjYW78MvFMZlXnKCHCJl9N2BPiBfYpS/5BRn6Vva8QjE8MaRTTvwBhgNWgWCCpfpmcolO4bp+zK5ManEi3jAvBAPJcMfi7lW7avmQoyTUVvgfnAsuQYyCSu3/qzVTBgWnUIMCAwEAAaOBojCBnzAfBgNVHSMEGDAWgBTX/RssoP3hDeglHLmlOrW/CJJ/JDAPBgNVHRMBAf8EBTADAQH/MA4GA1UdDwEB/wQEAwIBxjAdBgNVHQ4EFgQU1/0bLKD94Q3oJRy5pTq1vwiSfyQwPAYIKwYBBQUHAQEEMDAuMCwGCCsGAQUFBzABhiBodHRwOi8vaXBhLWNhLnBjYmwubG9jYWwvY2Evb2NzcDANBgkqhkiG9w0BAQsFAAOCAYEABf/N87OoG+iGbO11p6NLRIvbFqhkDIgCfsTT0EaSBgqmJ7zlkK7BEVN5jfICNjUUDjCL3k1LGzGJQ0QnEhnDpxfGDlKVACzgJqa322ZFKwT32CFrrPxcMpwkYH2Iz/WXEju/Fy2SH5+P9Pxk4RCJdb+PurRG/gGlUzOHeG91mj38Lz1G7McHJQhPNylFDt+6KK8q6t7wpUyISFdX9PKFYTFSVHGmos1akIk8s7IPWGxQIkPekaJVORHuPR/6LcSQopTdKAoj24Emg4XaxudHb2SOUktPrPpbPeUWkBfX+lTutQaOT9qh7hrG0WpXeTZ+Rub2iWYCtZukDeJp3sbRR+LZzrVrNJRBcsqmkSjq8QT9JIcwWfs67H4D3Fsl/gelbVIUYDvjqhd/1CVivQfd/5K2ptMxwFAsUqT5kMfUpxGxOjJSnSPtG1rzqshpMBwvVbb2ZdfjT49uqQIgAvU6rVNXPHqfJxfNhPmWHQCRScG5JolBDwiO0Tq/xfwWbnl9-----END CERTIFICATE-----" > /etc/pki/ca-trust/source/anchors/ca2.crt && update-ca-trust

RUN mkdir -p ~/.ssh && ssh-keyscan -t rsa -p 7999 bitbucket.pcbltools.ru >> ~/.ssh/known_hosts

#COPY --from=helm ./helm/bin/helm /usr/local/bin/

RUN helm plugin install https://github.com/chartmuseum/helm-push || true
RUN helm plugin install https://github.com/databus23/helm-diff || true
