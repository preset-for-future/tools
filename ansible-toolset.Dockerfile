FROM quay.io/ansible/toolset:3.4.1
RUN apt-get update && apt-get install -y --no-install-recommends \
    openssh-client \
  && rm -rf /var/lib/apt/lists/*
RUN mkdir -p ~/.ssh/ && echo 'Host *\n\
   StrictHostKeyChecking no\n\
   UserKnownHostsFile=/dev/null\n'\
> ~/.ssh/config
