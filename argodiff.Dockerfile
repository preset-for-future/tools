FROM harbor.sbereducation.site/quay.io/argoproj/argocd:v2.7.17

USER root

RUN apt-get update && apt-get install -y \
    colordiff \
    && rm -rf /var/lib/apt/lists/*

USER 999
